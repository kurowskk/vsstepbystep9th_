﻿using System;

namespace Vehicles
{
    class Program
    {
        void DoWork()
        {
            // TODO:
        }

        static void Main()
        {
            try
            {
                Program prog = new Program();
                prog.DoWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
